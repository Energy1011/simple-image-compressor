# simple-image-compressor (script)
Simple python3 script for image compression, using Image.save from PIL module with quality level, inspired from HolaMundo YTChannel
[Link](https://www.youtube.com/watch?v=sW4ScHICKtI)

Features: 
- Use logging
- Use argsparse
- New options (see next below)
- Requirements file (is it a feature ? :P)

Example using it: 
python3 compressor.py -q 60 -p ./custom_working_path -s custom_suffix -l DEBUG

# Usage:
```
usage: Image-Compressor by Energy1011 [-h] [-p MAIN_FOLDER] [-q QUALITY] [-s FILENAME_SUFFIX]
                                      [-l LOGLEVEL]

Simple python3 script for image compression, using Image.save from PIL module with quality
level

optional arguments:
  -h, --help            show this help message and exit
  -p MAIN_FOLDER, --path MAIN_FOLDER
                        Main path to work with
  -q QUALITY, --quality QUALITY
                        Integer number for lvl of quality saving with PIL example: 60 (default
                        image quality)
  -s FILENAME_SUFFIX, --suffix FILENAME_SUFFIX
                        Suffix for converted filename
  -l LOGLEVEL, --loglevel LOGLEVEL
                        Logging level: INFO, DEBUG, WARNING, CRITICAL, ERROR
```
