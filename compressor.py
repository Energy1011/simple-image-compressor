#!/bin/env python3.6
# -*- coding: utf-8 -*-
# Script for image compressor using PIL
# Copyright (C) 2022 Energy1011

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 
import os
import argparse
from PIL import Image
import emoji
import logging
AUTHOR = 'Energy1011' # Inspired in HolaMundo YTChannel
SCRIPT_NAME = "Image-Compressor"
DESCRIPTION = "Simple python3 script for image compression, using Image.save from PIL module with quality level"
APP = SCRIPT_NAME + ' by ' + AUTHOR
LOG = None

class Compresor:
    MAIN_FOLDER= "" 
    QUALITY = 60

    def __init__(self, args):
        self.MAIN_FOLDER = args.main_folder
        self.QUALITY = args.quality
        self.FILENAME_SUFFIX = args.filename_suffix

    def run(self):
        self.iter_main_folder()
        pass

    def iter_main_folder(self):
            for filename in os.listdir(self.MAIN_FOLDER):
                name, ext = os.path.splitext(self.MAIN_FOLDER + filename)
                if ext in [".jpg", ".jpeg", ".png"]:
                    self.compress(self.MAIN_FOLDER, filename, self.QUALITY)

    def compress(self, path, filename, quality):
        try:
            i = Image.open(path + filename)
            original_size = os.path.getsize(path + filename)
            i.save(path + self.FILENAME_SUFFIX + filename, optimize = True, quality = int(quality))
            new_size = os.path.getsize(path + self.FILENAME_SUFFIX + filename)
            LOG.info(emoji.emojize("\n🗜️  Compressing: {}{} \n\t-> (quality {}) from size {} Bytes to {} Bytes COMPLETE OK".format(path, filename, quality, format(original_size, ',d'), format(new_size, ',d'))))
        except Exception as e:
            LOG.info(emoji.emojize('\n:red_circle: Error {}'.format(e)))
            exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
    prog=APP, description = DESCRIPTION )
    parser.add_argument('-p', '--path', help="Main path to work with",  dest='main_folder', type = str, default = os.getcwd() + os.sep)
    parser.add_argument('-q', '--quality', help="Integer number for lvl of quality saving with PIL example: 60 (default image quality)",  
        type = int, dest='quality', default = 60)
    parser.add_argument('-s', '--suffix', help="Suffix for converted filename",  dest='filename_suffix', type= str, default = 'compressed_')
    parser.add_argument('-l', '--loglevel', dest='loglevel', default=logging.INFO, help='Logging level: INFO, DEBUG, WARNING, CRITICAL, ERROR')
    args = parser.parse_args()

    # Init logging for this module
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=args.loglevel) 
    LOG = logging.getLogger(APP)

    script = Compresor(args)
    script.run()            
    exit(0)
